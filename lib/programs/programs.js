 "use strict";

const path = require("path");
const fs = require("fs-extra");
const chokidar = require("chokidar");
const colors = require("colors/safe");

const BASEPATH = process.env.PWD;

let _vez = 0;
let _file_loader = null;
let _watcherFiles = null;
let _callBackDone = false;

module.exports = {
	start
}

function start()
{
	console.log(
		colors.blue("\n------------------------------------------"),
		colors.blue("\n--------- LOAD PRESET-FILE-LOADER --------"),
		colors.blue("\n------------------------------------------"),
	);

	// GET CONFIG
	getData(watchFiles);
}

function getData(callBack)
{
	chokidar.watch(BASEPATH, {
		ignored: BASEPATH + "/node_modules",
		persistent: true
	})
		.on("all", function (event, curPath)
		{
			curPath = curPath.replace(/\\/g, "/");

			if (curPath == BASEPATH + "/package.json" || curPath == BASEPATH + "/.file-loader.json")
			{
				console.log(
					colors.blue("\nCHANGE: " + (++_vez)) +
					colors.white("\nevent: " + event) +
					colors.white("\nfunction: getData") +
					colors.white("\ncurPath: " + curPath)
				);

				if (curPath == BASEPATH + "/package.json")
					_file_loader = fs.readJsonSync(BASEPATH + "/package.json")["file-loader"];
				else
					_file_loader = fs.readJsonSync(BASEPATH + "/.file-loader.json");

				if (_watcherFiles)
				{
					_callBackDone = false;
					_watcherFiles.close();
				}

				if (!_callBackDone && typeof callBack == "function" && _file_loader)
				{
					_callBackDone = true;
					callBack();
				}
			}
		});
}

function watchFiles()
{
	if (_file_loader)
	{
		// START SERVICE
		console.log(
			colors.green("\n------------------------------------------"),
			colors.green("\n-------- START PRESET-FILE-LOADER --------"),
			colors.green("\n------------------------------------------"),
			colors.green("\n\n_file_loader.baseFolder: " + _file_loader.baseFolder),
			colors.green("\n_file_loader.newFolder: " + _file_loader.newFolder),
			colors.green("\n_file_loader.extensions: " + JSON.stringify(_file_loader.extensions)),
			colors.green("\nfullPath: " + (BASEPATH + _file_loader.baseFolder)),
		);

		_watcherFiles = chokidar.watch(BASEPATH + _file_loader.baseFolder);

		_watcherFiles.on("all", function (event, curPath)
		{
			curPath = curPath.replace(/\\/g, "/");

			if (path.extname(curPath) && !_file_loader.extensions.includes(path.extname(curPath))) return;

			console.log(
				colors.blue("\nCHANGE: " + (++_vez)) +
				colors.white("\nevent: " + event) +
				colors.white("\nfunction: watchFiles") +
				colors.white("\ncurPath: " + curPath)
			);

			if (event == "add" || event == "addDir")
			{
				console.log(colors.white(`copySync: ${BASEPATH}${_file_loader.newFolder}${curPath.replace(BASEPATH + _file_loader.baseFolder, "")}`));
				fs.copySync(curPath, BASEPATH + _file_loader.newFolder + curPath.replace(BASEPATH + _file_loader.baseFolder, ""));
			}
			else if (event == "unlink" || event == "unlinkDir")
			{
				if (fs.existsSync(BASEPATH + _file_loader.newFolder + curPath.replace(BASEPATH + _file_loader.baseFolder, "")))
				{
					console.log(colors.white(`rmdirSync: ${BASEPATH}${_file_loader.newFolder}${curPath.replace(BASEPATH + _file_loader.baseFolder, "")}`));
					fs.removeSync(BASEPATH + _file_loader.newFolder + curPath.replace(BASEPATH + _file_loader.baseFolder, ""));
				}
			}
		});
	}
	else
	{
		console.log(colors.red("\nNão foram encontradas as configurações de PRESET-FILE-LOADER: " +
			"\n\n" + BASEPATH + "/package.json" +
			"\n" + BASEPATH + "/.file-loader.json")
		);

		return null;
	}
}
